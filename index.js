let myTrainer = {
	name: 'Ash',
	age: 18,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log()
		console.log('Pikachu! I choose You!')
	}
}


		let myPokemon = {
				name: 'Pikachu',
				level: 3,
				health: 100,
				attack: 50,
		}


		function Pokemon(name, level, health, attack){

		// Properties
			this.name = name;
			this.level = level;
			this.health = 2 * level;
			this.attack = level;
			
		// Methods
			this.tackle = function(target){
				console.log(this.name + ' tackled ' + target.name);
				console.log(target.name + "'s health is now reduced to " + Number(target.health - this.health));
				
				
				}
					
			this.faint = function(){
				console.log(this.name + ' fainted.');
			};
				
			
		}

	console.log(myTrainer);
	console.log('Result from dot notation');
	console.log(myTrainer.name);

	console.log('Result from square bracket notation');
	console.log(myTrainer['pokemon']);


	myTrainer.talk();

	let pikachu = new Pokemon('Pikachu', 16, 100, 50);
	let charizard = new Pokemon('Charizard', 10, 100, 30); 
	let	may = new Pokemon('May', 10, 100, 45);
	let brock = new Pokemon('Brock', 12, 100, 40);

	console.log(pikachu);
	console.log(charizard);
	console.log(may);
	console.log(brock);

	pikachu.tackle(may);
		may.faint();

